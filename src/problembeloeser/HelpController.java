package problembeloeser;

import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TabPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class HelpController {

    // Controllerklasse für das Hilfefenster

    @FXML
    private ScrollPane grundlagenBox, anleitungsBox;
    @FXML
    private TabPane hilfeTab;

    @FXML
    public void initialize() {
        // Hilfstexte wurden in eigene Dateien ausgelagert, da ich sie nicht mit in einre Quelltextdatei speichern wollte
        setContent(grundlagenBox, "src/problembeloeser/grundlagentext.txt");
        setContent(anleitungsBox, "src/problembeloeser/hilfetext.txt");
    }

    private void setContent(ScrollPane box, String filePath) {
        Text inhalt = new Text();
        inhalt.setTextAlignment(TextAlignment.JUSTIFY);
        box.setContent(inhalt);
        inhalt.wrappingWidthProperty().bind(hilfeTab.widthProperty().subtract(20));

        // FileReader-Block: Hier wird eine Datei ausgelesen, wobei diverse Einlesefehler als IOException abgefangen werden
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader dataReader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), StandardCharsets.UTF_8));
            String line;
            while ((line = dataReader.readLine()) != null) {
                text.append("".equals(text.toString()) ? "" : "\n");
                text.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        inhalt.setText(text.toString());
    }

}
