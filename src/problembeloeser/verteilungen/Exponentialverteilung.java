package problembeloeser.verteilungen;

public class Exponentialverteilung extends Verteilung {
	
	public Exponentialverteilung(double erwartungswert) {
		super(erwartungswert);
	}
	
	@Override
	public double getZufall() {
		return Math.max(-this.erwartungswert * Math.log(this.rand.nextDouble()), 0);
	}
}
