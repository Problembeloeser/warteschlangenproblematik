package problembeloeser.verteilungen;

public class Konstantverteilung extends Verteilung {
	
	public Konstantverteilung(double erwartungswert){
		super(erwartungswert);
	}
	
	@Override
	public double getZufall() {
		return this.erwartungswert;
	}
}
