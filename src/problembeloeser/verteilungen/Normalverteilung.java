package problembeloeser.verteilungen;

public class Normalverteilung extends Verteilung {
	
	private double standardAbweichung;
	
	public Normalverteilung(double erwartungswert) {
		this(erwartungswert, Math.sqrt(erwartungswert));
	}
	
	public Normalverteilung(double erwartungswert, double standardAbweichung) {
		super(erwartungswert);
		this.standardAbweichung = standardAbweichung;
	}
	
	public void setStandardAbweichung(double standardAbweichung) {
		this.standardAbweichung = standardAbweichung;
	}
	
	@Override
	public double getZufall() {
		return Math.max(this.rand.nextGaussian() * this.standardAbweichung + this.erwartungswert, 0);
	}
}
