package problembeloeser.verteilungen;

import java.util.Random;

public abstract class Verteilung {

	/*
		Verteilung ist die Abstrakte Klasse, die dazu dient, um Verteilungen zu generalisieren. Sie gibt die Vorlagen, die jede Verteilung nutzen muss.
	 */

	protected double erwartungswert;
	protected Random rand;
	
	public Verteilung(double erwartungswert) {
		this.erwartungswert = erwartungswert;
		this.rand = new Random();
	}
	
	public abstract double getZufall();
	
	public double getErwartung() {return this.erwartungswert;}
	
	public void setErwartung(double erwartungswert) {
		this.erwartungswert = erwartungswert;
	}
	
}
