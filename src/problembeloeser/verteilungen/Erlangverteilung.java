package problembeloeser.verteilungen;

public class Erlangverteilung extends Verteilung {
	
	private Exponentialverteilung grundlage;
	private int potenz;
	
	public Erlangverteilung(double erwartungswert, int potenz) {
		super(erwartungswert);
		this.grundlage = new Exponentialverteilung(erwartungswert);
		this.potenz = potenz;
	}
	
	@Override
	public double getZufall() {
		double val = 0;
		for (int i = 0; i < this.potenz; i++){
			val += this.grundlage.getZufall();
		}
		return Math.max(val, 0);
	}
}
