package problembeloeser.verteilungen;

public class Gleichverteilung extends Verteilung {
	
	public Gleichverteilung(double erwartungswert){
		super(erwartungswert);
	}
	
	@Override
	public double getZufall() {
		return this.rand.nextDouble() * 2 * this.erwartungswert;
	}
}
