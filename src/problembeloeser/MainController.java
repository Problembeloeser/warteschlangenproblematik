package problembeloeser;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import problembeloeser.verteilungen.*;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainController implements BearbeitungsListener {

    // Controllerklasse für das Hauptfenster

    @FXML
    private Pane anzeigePanel;
    @FXML
    private Label ankunftLabel, anzahlLabel, bearbeitungsLabel;
    @FXML
    private Slider ankunftSlider, anzahlSlider, bearbeitungsSlider;
    @FXML
    private ChoiceBox<String> ankunftBox, bearbeitungBox;
    @FXML
    private RadioMenuItem pauseRadio;
    @FXML
    private ToggleGroup group;

    private Wartesystem wartesystem;
    private Group einheiten, wartende, fertige, einheitenCounter;
    private Text counter, warteGesamtMenge, warteGesamtZeit, fertigGesamtMenge, fertigGesamtZeit;
    private SimpleDoubleProperty counterWidthProperty, fertigZeitProperty, fertigMengeProperty;
    private double warteSumme;
    private Timeline updateLine;
    private final double frequenz = 10; // Festgelegt, aber prinzipiell variabel. Ich habe bisher keinen Grund gesehen, eine Einstellung dafür zu implementieren
    private final DecimalFormat formatierung = new DecimalFormat("0.0");

    @FXML
    private void startTimeline() {
        updateLine.play();
    }

    @FXML
    private void pauseTimeline() {
        updateLine.pause();
    }

    @FXML
    private void resetSimulation() {
        // Simulation zurücksetzen
        wartesystem.reset();
        wartende.getChildren().clear();
        fertige.getChildren().clear();
        drawBearbeitung();
        updateLine.pause();
        pauseRadio.setSelected(true);
        drawWarteliste();
        drawFertige();
        counter.setText("0.0 s");
    }

    @FXML
    public void openHelp() {
        // Hilfe öffnen
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("help.fxml"));
            Stage help = new Stage();
            help.setTitle("Hilfe");
            help.setScene(new Scene(root, 800, 600));
            help.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void komponentenEinrichten(){
        anzahlLabel.textProperty().bind(Bindings.concat("n=").concat(anzahlSlider.valueProperty().asString("%.0f").concat(": ")));
        anzahlSlider.valueChangingProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                anzahlSlider.setValue(Math.round(anzahlSlider.getValue()));
                anzahlSlider.setMax(Math.max((int) anzahlSlider.getValue() + 5, 10));
                anzahlSlider.setMin(Math.max((int) anzahlSlider.getValue() - 5, 1));
            }
        });

        wartesystem = new Wartesystem(new Konstantverteilung(1.0 / 50.0), new Konstantverteilung(1.0 / 50.0), 0, Wartesystem.REIHENFOLGE_FIFO);
        wartesystem.addListener(this);

        einheiten = new Group();
        wartende = new Group();
        fertige = new Group();
        einheitenCounter = new Group();

        counter = new Text();
        counter.setText("0.0 s");
        Tooltip.install(counter, new Tooltip("Dauer bis zur Ankunft des nächsten Objektes"));

        warteGesamtMenge = new Text();
        warteGesamtZeit = new Text();
        fertigGesamtMenge = new Text();
        fertigGesamtZeit = new Text();

        warteGesamtMenge.xProperty().bind(anzeigePanel.widthProperty().multiply(0.025));
        warteGesamtMenge.yProperty().bind(anzeigePanel.heightProperty().multiply(0.25).add(anzeigePanel.widthProperty().divide(40)));
        warteGesamtMenge.wrappingWidthProperty().bind(anzeigePanel.widthProperty().multiply(0.15));
        Tooltip.install(warteGesamtMenge, new Tooltip("Anzahl von Objekten in der Wartesystem"));

        warteGesamtZeit.xProperty().bind(anzeigePanel.widthProperty().multiply(0.825));
        warteGesamtZeit.yProperty().bind(warteGesamtMenge.yProperty());
        warteGesamtZeit.wrappingWidthProperty().bind(anzeigePanel.widthProperty().multiply(0.15));
        Tooltip.install(warteGesamtZeit, new Tooltip("Gesamtdauer der Objekte in der Wartesystem"));

        fertigGesamtMenge = new Text();
        fertigGesamtZeit = new Text();

        fertigZeitProperty = new SimpleDoubleProperty(fertigGesamtZeit.getLayoutBounds().getWidth());
        fertigMengeProperty = new SimpleDoubleProperty(fertigGesamtMenge.getLayoutBounds().getWidth());
        counterWidthProperty = new SimpleDoubleProperty(counter.getLayoutBounds().getWidth());

        counter.xProperty().bind(anzeigePanel.widthProperty().subtract(counterWidthProperty).multiply(0.5));
        counter.yProperty().bind(anzeigePanel.heightProperty().multiply(0.25).add(anzeigePanel.widthProperty().divide(20)).add(15));

        fertigGesamtMenge.xProperty().bind(anzeigePanel.widthProperty().subtract(fertigMengeProperty).divide(2));
        fertigGesamtZeit.xProperty().bind(anzeigePanel.widthProperty().subtract(fertigZeitProperty).divide(2));
        fertigGesamtMenge.yProperty().bind(anzeigePanel.heightProperty().multiply(0.8));
        fertigGesamtZeit.yProperty().bind(fertigGesamtMenge.yProperty().add(15));
        Tooltip.install(fertigGesamtMenge, new Tooltip("Anzahl der bereits fertigen Objekte"));
        Tooltip.install(fertigGesamtZeit, new Tooltip("Gesamtdauer abgefertigter Objekte"));

        Rectangle warteHintergrund = new Rectangle();
        warteHintergrund.setFill(Color.grayRgb(100));
        warteHintergrund.yProperty().bind(anzeigePanel.heightProperty().multiply(0.25).subtract(5));
        warteHintergrund.setX(0);
        warteHintergrund.widthProperty().bind(anzeigePanel.widthProperty());
        warteHintergrund.heightProperty().bind(anzeigePanel.widthProperty().divide(20).add(25));
        Tooltip.install(warteHintergrund, new Tooltip("Wartesystem"));
        anzeigePanel.getChildren().add(warteHintergrund);

        Rectangle fertigHintergrund = new Rectangle();
        fertigHintergrund.setFill(Color.grayRgb(100));
        fertigHintergrund.yProperty().bind(fertigGesamtMenge.yProperty().subtract(20));
        fertigHintergrund.setX(0);
        fertigHintergrund.widthProperty().bind(anzeigePanel.widthProperty());
        fertigHintergrund.setHeight(40);
        Tooltip.install(fertigHintergrund, new Tooltip("Fertige Objekte"));
        anzeigePanel.getChildren().add(fertigHintergrund);

        anzeigePanel.getChildren().addAll(einheiten, wartende, fertige, counter, einheitenCounter, warteGesamtMenge, warteGesamtZeit, fertigGesamtMenge, fertigGesamtZeit);
    }

    @FXML
    public void initialize() {
        /*
         initialize() wird aufgerufen, nachdem das Fenster mit seinen Komponenten aus der FXML-Datei geladen wurde und sorgt für die Initialisierung aller Komponenten
         Es ist also quasi der Constructor des GUI
         */
        initSliderAnpassung(ankunftLabel, ankunftSlider);
        initSliderAnpassung(bearbeitungsLabel, bearbeitungsSlider);

        komponentenEinrichten();

        choiceBoxHandler();
        sliderHandler();
        bindAnzahlSlider();
        drawEinheiten((int) anzahlSlider.getValue());

        drawWarteliste();
        drawFertige();

        updateLine = new Timeline(new KeyFrame(Duration.seconds(1.0 / frequenz), event -> tickSimulation()));
        updateLine.setCycleCount(Timeline.INDEFINITE);

        group.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                switch (((RadioMenuItem) newValue).getText()) {
                    case "LiFo":
                        wartesystem.setReihenfolge(Wartesystem.REIHENFOLGE_LIFO);
                        break;
                    case "nach Dauer aufsteigend":
                        wartesystem.setReihenfolge(Wartesystem.REIHENFOLGE_AUFSTEIGEND);
                        break;
                    case "nach Dauer absteigend":
                        wartesystem.setReihenfolge(Wartesystem.REIHENFOLGE_ABSTEIGEND);
                        break;
                    case "Zufällig":
                        wartesystem.setReihenfolge(Wartesystem.REIHENFOLGE_ZUFALL);
                        break;
                    default:
                        //FiFo ist Standard
                        wartesystem.setReihenfolge(Wartesystem.REIHENFOLGE_FIFO);
                        break;
                }
            }
        });

    }

    private void calcWartesumme() {
        // Für die Skalierung der Objekte in der Wartesystem entsprechend ihrer Dauer
        warteSumme = 0;
        for (Gegenstand slot : wartesystem.getWartendeKunden()) {
            warteSumme += slot.getDauer();
        }
    }

    private void drawWarteliste() {
        // Wird aufgerufen, sobald eine Änderung in der Warteliste auftritt, um diese zu aktualisieren
        calcWartesumme();
        wartende.getChildren().clear();
        ArrayList<Gegenstand> wartende = wartesystem.getWartendeKunden();
        Rectangle[] objekte = new Rectangle[wartende.size()];
        for (int i = 0; i < wartende.size(); i++) {
            objekte[i] = new Rectangle();
            objekte[i].setFill(wartende.get(i).getFarbe());
            objekte[i].heightProperty().bind(anzeigePanel.widthProperty().divide(20.0));
            objekte[i].widthProperty().bind(anzeigePanel.widthProperty().multiply(0.6).multiply(wartende.get(i).getDauer()).divide(warteSumme));
            objekte[i].yProperty().bind(anzeigePanel.heightProperty().multiply(0.25));
            objekte[i].xProperty().bind(i != 0 ? objekte[i - 1].xProperty().add(objekte[i - 1].widthProperty()) : anzeigePanel.widthProperty().multiply(0.2));
        }
        this.wartende.getChildren().addAll(objekte);
        warteGesamtMenge.setText("Anzahl: " + wartende.size());
        warteGesamtZeit.setText("Dauer: " + formatierung.format(warteSumme * 60) + " s");
    }

    private void drawBearbeitung() {
        // Farbanpassung der Bearbeitungseinheiten
        Gegenstand[] currentBearbeitung = wartesystem.getCurrentBearbeitung();
        for (int i = 0; i < currentBearbeitung.length; i++) {
            ((Rectangle) einheiten.getChildren().get(i)).setFill(currentBearbeitung[i] != null ? currentBearbeitung[i].getFarbe() : Color.BLACK);
            ((Text) einheitenCounter.getChildren().get(i)).setText((currentBearbeitung[i] != null ? formatierung.format(currentBearbeitung[i].getNochDauer() * 60) : "0.0") + " s");
        }
    }

    private void drawFertige() {
        // Aktualisierung der Anzeige von bereits fertigen Objekten
        fertigGesamtZeit.setText("Dauer: " + formatierung.format(wartesystem.getFertigSumme() * 60) + " s");
        fertigGesamtMenge.setText("Anzahl: " + wartesystem.getFertigAnzahl());
        fertigMengeProperty.set(fertigGesamtMenge.getLayoutBounds().getWidth());
        fertigZeitProperty.set(fertigGesamtZeit.getLayoutBounds().getWidth());
    }

    private void drawEinheiten(int anzahl) {
        // Initialisierung der Serviceeinheiten
        wartende.getChildren().clear();
        fertige.getChildren().clear();
        einheiten.getChildren().clear();
        einheitenCounter.getChildren().clear();
        Rectangle[] einheiten = new Rectangle[anzahl];
        Text[] counter = new Text[anzahl];
        for (int i = 0; i < anzahl; i++) {
            einheiten[i] = new Rectangle();
            einheiten[i].setFill(Color.BLACK);
            String einheitenStyle = "-fx-border-color: #ffffff;"
                    + "-fx-border-width: 5pt;";
            einheiten[i].setStyle(einheitenStyle);
            einheiten[i].widthProperty().bind(anzeigePanel.widthProperty().divide(20.0));
            einheiten[i].heightProperty().bind(einheiten[i].widthProperty());
            einheiten[i].yProperty().bind(anzeigePanel.heightProperty().multiply(0.5));
            einheiten[i].xProperty().bind(anzeigePanel.widthProperty().multiply(i + 1).divide(anzahl + 1).subtract(einheiten[i].widthProperty().divide(2)));
            Tooltip.install(einheiten[i], new Tooltip("Einheit " + (i + 1)));

            counter[i] = new Text();
            counter[i].xProperty().bind(einheiten[i].xProperty());
            counter[i].yProperty().bind(einheiten[i].yProperty().add(einheiten[i].heightProperty()).add(15));
            counter[i].setText("0.0 s");
            Tooltip.install(counter[i], new Tooltip("Verbleibende Bearbeitungsdauer in Einheit " + (i + 1)));
        }
        this.einheitenCounter.getChildren().addAll(counter);
        this.einheiten.getChildren().addAll(einheiten);
        wartesystem = new Wartesystem(wartesystem.getAnkunftsVerteilung(), wartesystem.getBearbeitungsVerteilung(), anzahl, wartesystem.getReihenfolge());
        wartesystem.addListener(this);
    }

    private void tickSimulation() {
        // Wird von der Timeline gecallt und dient der Aktualisierung der Wartesystem
        wartesystem.tick(1.0 / (60.0 * frequenz));
        counter.setText(formatierung.format(wartesystem.getTime2NextAnkunft() * 60.0) + " s");
        counterWidthProperty.set(counter.getLayoutBounds().getWidth());
        drawBearbeitung();
    }

    private void bindAnzahlSlider() {
        anzahlSlider.valueProperty().addListener((observable, oldValue, newValue) -> drawEinheiten(newValue.intValue()));
    }

    private void choiceBoxHandler() {
        // Ermöglicht Änderung der Verteilungsfunktion in Laufzeit
        ankunftBox.valueProperty().addListener((observable, oldValue, newValue) -> wartesystem.setAnkunftsVerteilung(genNeueVerteilung(newValue, ankunftSlider)));
        bearbeitungBox.valueProperty().addListener((observable, oldValue, newValue) -> wartesystem.setBearbeitungsVerteilung(genNeueVerteilung(newValue, bearbeitungsSlider)));
    }

    private Verteilung genNeueVerteilung(String newValue, Slider slider) {
        // Erzeugt neue Verteilungsfunktion
        double erwartungswert = 1.0 / slider.getValue();
        switch (newValue) {
            case "Normalverteilung":
                return new Normalverteilung(erwartungswert);
            case "Gleichverteilung":
                return new Gleichverteilung(erwartungswert);
            case "Exponentialverteilung":
                return new Exponentialverteilung(erwartungswert);
            case "Erlangverteilung (n=5)":
                return new Erlangverteilung(erwartungswert, 5);
            default:
                //Konstantverteilung ist Standard
                return new Konstantverteilung(erwartungswert);
        }
    }

    private void sliderHandler() {
        // Bindet Erwartungswert an Eingabe
        ankunftSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            wartesystem.getAnkunftsVerteilung().setErwartung(1.0 / newValue.doubleValue());
            wartesystem.updateAnkunft();
        });
        bearbeitungsSlider.valueProperty().addListener((observable, oldValue, newValue) -> wartesystem.getBearbeitungsVerteilung().setErwartung(1.0 / newValue.doubleValue()));
    }

    private void initSliderAnpassung(Label label, Slider slider) {
        // Sorgt dafür, dass der Eingabebereich des Erwartungswertes dynamisch ist.
        label.textProperty().bind(Bindings.concat("μ=").concat(slider.valueProperty().asString("%.1f")));
        slider.valueChangingProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                slider.setValue(Math.round(slider.getValue() * 10.0) / 10.0);
                slider.setMax(Math.max(slider.getValue() * 2.0, 2.0));
            }
        });
        slider.majorTickUnitProperty().bind(slider.maxProperty().divide(2.0));
    }

    @Override
    public void update(String message) {
        // Handler für die ActionEvents von
        switch (message) {
            case "Fertig":
                drawBearbeitung();
                drawFertige();
                break;
            case "Bearbeite":
                drawWarteliste();
                drawBearbeitung();
                break;
            case "Ankunft":
                drawWarteliste();
                break;
        }
    }

}
