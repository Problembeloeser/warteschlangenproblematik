package problembeloeser;

import problembeloeser.verteilungen.Verteilung;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Wartesystem {

    // Diese Klasse verwaltet die Funktionen der Wartesystem. Sie ist als Bestandteil der OOP dafür da, Grafikinterface von Funktionalität zu trennen.

    static final byte REIHENFOLGE_FIFO = 0;
    static final byte REIHENFOLGE_LIFO = 1;
    static final byte REIHENFOLGE_ZUFALL = 2;
    static final byte REIHENFOLGE_AUFSTEIGEND = 3;
    static final byte REIHENFOLGE_ABSTEIGEND = 4;

    private int anzahl, fertigAnzahl;
    private Verteilung ankunftsVerteilung;
    private double time2NextAnkunft;
    private Verteilung bearbeitungsVerteilung;
    private Gegenstand[] currentBearbeitung;
    private ArrayList<Gegenstand> wartendeKunden;
    private double fertigSumme;
    private byte reihenfolge;
    private List<BearbeitungsListener> listeners = new ArrayList<>();

    //Verschachtelte Konstruktoren ermöglichen Objekterstellung mit weniger Parametern

    public Wartesystem(Verteilung ankunftsVerteilung, Verteilung bearbeitungsVerteilung) {
        this(ankunftsVerteilung, bearbeitungsVerteilung, 1, REIHENFOLGE_FIFO);
    }

    public Wartesystem(Verteilung ankunftsVerteilung, Verteilung bearbeitungsVerteilung, int anzahl) {
        this(ankunftsVerteilung, bearbeitungsVerteilung, anzahl, REIHENFOLGE_FIFO);
    }

    public Wartesystem(Verteilung ankunftsVerteilung, Verteilung bearbeitungsVerteilung, int anzahl, byte reihenfolge) {
        this.anzahl = anzahl;
        this.reihenfolge = reihenfolge;

        this.ankunftsVerteilung = ankunftsVerteilung;
        this.bearbeitungsVerteilung = bearbeitungsVerteilung;
        this.currentBearbeitung = new Gegenstand[this.anzahl];
        this.wartendeKunden = new ArrayList<>();
        this.fertigSumme = 0;
        this.fertigAnzahl = 0;

        this.time2NextAnkunft = ankunftsVerteilung.getZufall();
    }

    void updateAnkunft() {
        // Generalisierung der Verteilungsfunktion
        this.time2NextAnkunft = ankunftsVerteilung.getZufall();
    }

    void addListener(BearbeitungsListener listener) {
        // ActionListener hinzufügen
        listeners.add(listener);
    }

    private void callEvent(String message) {
        // ActionEvent an ActionListener weitergeben
        for (BearbeitungsListener listener : listeners) {
            listener.update(message);
        }
    }

    void reset() {
        // Reset der Wartesystem ohne ein neues Objekt zu generieren
        time2NextAnkunft = ankunftsVerteilung.getZufall();
        wartendeKunden.clear();
        currentBearbeitung = new Gegenstand[currentBearbeitung.length];
        fertigAnzahl = 0;
        fertigSumme = 0;
    }

    private void checkeFertige(double dauer) {
        // Alle Serviceeinheiten prüfen, ob ein Objekt fertig ist und dabei alle Objekte weiter bearbeiten
        for (int i = 0; i < currentBearbeitung.length; i++) {
            if (currentBearbeitung[i] != null) {
                currentBearbeitung[i].tickDauer(dauer);
                if (currentBearbeitung[i].getNochDauer() <= 0) {
                    fertigAnzahl++;
                    fertigSumme += currentBearbeitung[i].getDauer();
                    callEvent("Fertig");
                    currentBearbeitung[i] = null;
                    tryRefill();
                }
            }
        }
    }

    private int getWartender() {
        // Implementierung der Servicedisziplin
        int slot;
        switch (this.reihenfolge) {
            case REIHENFOLGE_LIFO:
                return wartendeKunden.size() - 1;
            case REIHENFOLGE_AUFSTEIGEND:
                double min = Double.MAX_VALUE;
                slot = 0;
                for (int i = 0; i < wartendeKunden.size(); i++) {
                    if (wartendeKunden.get(i).getDauer() < min) {
                        min = wartendeKunden.get(i).getDauer();
                        slot = i;
                    }
                }
                return slot;
            case REIHENFOLGE_ABSTEIGEND:
                Gegenstand max = new Gegenstand(0);
                slot = 0;
                for (int i = 0; i < wartendeKunden.size(); i++) {
                    if (wartendeKunden.get(i).getDauer() > max.getDauer()) {
                        max = wartendeKunden.get(i).clone();
                        slot = i;
                    }
                }
                return slot;
            case REIHENFOLGE_ZUFALL:
                Random rand = new Random();
                return rand.nextInt(wartendeKunden.size());
        }
        //FiFo ist Standard
        return 0;
    }

    private void tryRefill() {
        // Nachfüllen der Serviceeinheiten, wenn möglich
        for (int i = 0; i < currentBearbeitung.length; i++) {
            if (currentBearbeitung[i] == null && wartendeKunden.size() > 0) {
                int wartender = getWartender();
                currentBearbeitung[i] = wartendeKunden.get(wartender);
                wartendeKunden.remove(wartender);
                callEvent("Bearbeite");
            }
        }
    }

    void tick(double delta) {
        // Wartesystem mit bestimmter Zeitdauer aktualisieren
        time2NextAnkunft -= delta;
        while (time2NextAnkunft <= 0) {
            Gegenstand neuer = new Gegenstand(bearbeitungsVerteilung);
            wartendeKunden.add(neuer);
            time2NextAnkunft += ankunftsVerteilung.getZufall();
            callEvent("Ankunft");
        }
        checkeFertige(delta);
        tryRefill();
    }

    Verteilung getAnkunftsVerteilung() {
        return ankunftsVerteilung;
    }

    Verteilung getBearbeitungsVerteilung() {
        return bearbeitungsVerteilung;
    }

    void setAnkunftsVerteilung(Verteilung ankunftsVerteilung) {
        this.ankunftsVerteilung = ankunftsVerteilung;
    }

    void setBearbeitungsVerteilung(Verteilung bearbeitungsVerteilung) {
        this.bearbeitungsVerteilung = bearbeitungsVerteilung;
    }

    ArrayList<Gegenstand> getWartendeKunden() {
        return this.wartendeKunden;
    }

    Gegenstand[] getCurrentBearbeitung() {
        return this.currentBearbeitung;
    }

    @SuppressWarnings("unused")
    public int getAnzahl() {
        return this.anzahl;
    }

    void setReihenfolge(byte reihenfolge) {
        this.reihenfolge = reihenfolge;
    }

    double getFertigSumme() {
        return fertigSumme;
    }

    double getFertigAnzahl() {
        return fertigAnzahl;
    }

    byte getReihenfolge() {
        return this.reihenfolge;
    }

    double getTime2NextAnkunft() {
        return this.time2NextAnkunft;
    }

}
