package problembeloeser;

import javafx.scene.paint.Color;
import problembeloeser.verteilungen.Verteilung;

import java.io.Serializable;

public class Gegenstand implements Cloneable, Serializable {

	// Klasse, welche die Objekte verwaltet, sowohl Darstellungsfarbe als auch Dauer

	private double dauer;
	private double nochDauer;
	private Color grundFarbe;
	
	public Gegenstand(Verteilung dauerverteilung) {
		this.dauer = dauerverteilung.getZufall();
		this.nochDauer = dauer;
		do {
			this.grundFarbe = Color.color(Math.random(), Math.random(), Math.random());
		} while (this.grundFarbe.getBrightness() < 0.1 || this.grundFarbe.getBrightness() > 0.9);
	}
	
	
	public Color getFarbe() {
		// Bearbeitungszustand grafisch darstellen
		return grundFarbe.interpolate(Color.BLACK, 1 - nochDauer / dauer);
	}

	@SuppressWarnings("unused")
	public void setGrundFarbe(Color farbe) {
		this.grundFarbe = farbe;
	}
	
	public Gegenstand(double dauer) {
		this.dauer = dauer;
	}
	
	public double getNochDauer() {
		return this.nochDauer;
	}
	
	public double getDauer() {
		return this.dauer;
	}
	
	public void tickDauer(double dauer) {
		this.nochDauer = Math.max(0, this.nochDauer - dauer);
	}

	@Deprecated
	@Override
	public Gegenstand clone() {
		// Für eine eventuelle Weitergabe eines fertigen Objektes in eine Liste fertiger Objekte. Zugunsten der Performance durch einfach Zahlen ersetzt
		return new Gegenstand(dauer);
	}
	
	@Override
	public String toString() {
		// Kommt aus Debugging
		return "Gegenstand " + this.dauer;
	}
	
}
